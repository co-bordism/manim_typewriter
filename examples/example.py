from manim import *
from manim_typewriter import *


class TypeWriterDemo(Scene):
    def construct(self):
        the_text = Tex(
            "Typing text with a typewriter?",
            tex_template=TexFontTemplates.latin_modern_tw,
        )
        self.play(TypeWriter(the_text, time_per_letter=0.2))
        self.wait(1)