from manim import *


class Type(Transform):
    """
    A creation animation that fades in and scales down. It is meant to represent
    a single letter being typed by a typewriter
    """

    def __init__(self, text, run_time=0.25, **kwargs):
        Transform.__init__(
            self,
            text,
            run_time=run_time,
            rate_func=rate_functions.ease_in_out_circ,
            **kwargs,
        )

    def create_target(self):
        target = self.mobject.copy()
        return target

    def begin(self):
        super().begin()
        self.starting_mobject.scale_in_place(2)
        self.starting_mobject.fade(1)


class LetterByLetter(AnimationGroup):
    def __init__(self, letters, time_per_letter=0.25, lag_ratio=0.9, **kwargs):
        n = len(letters)
        anims = [Type(letters[i], run_time=time_per_letter) for i in range(n)]
        AnimationGroup.__init__(
            self,
            *anims,
            lag_ratio=lag_ratio,
            run_time=time_per_letter * lag_ratio * n,
        )


def TypeWriter(text, **kwargs):
    if isinstance(text, MathTex):  # as opposed to just SingleStringMathTex
        letters = [text[i][j] for i in range(len(text)) for j in range(len(text[i]))]
    else:
        letters = text

    return LetterByLetter(letters, **kwargs)